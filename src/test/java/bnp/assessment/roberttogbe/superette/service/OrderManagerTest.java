
package bnp.assessment.roberttogbe.superette.service;

import bnp.assessment.roberttogbe.superette.dao.FakeOrderFruitDaoAccessService;
import bnp.assessment.roberttogbe.superette.model.Apple;
import bnp.assessment.roberttogbe.superette.model.Fruit;
import bnp.assessment.roberttogbe.superette.model.Orange;
import bnp.assessment.roberttogbe.superette.model.Watermelon;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.is;


@RunWith(SpringJUnit4ClassRunner.class)
public class OrderManagerTest {

    private FakeOrderFruitDaoAccessService underTest;

    private static final double PRECISION = 0.000001;

    @Before
    public void setUp() {
        underTest = new FakeOrderFruitDaoAccessService();
        underTest.clearDB();
    }

    @Test
    public void shouldCorrectlyApplyOfferOnApplesWatermelonsOranges() {
        //Given
        // an apple called "Granit" with an id: 10, a quantity: 7 an unitPrice 0.20
        //an watermelon called "Malimelon" with an id: 20, a quantity: 6 an unitPrice 0.80
        //an orange called "Bukavu" with an id: 30, a quantity: 8 an unitPrice 0.50

        Fruit apple = new Apple(10, "myapple",7,0.20);
        Fruit  watermelon = new Watermelon(20,"melon",6,0.80);
        Fruit  orange = new Orange(30,"orange",8,0.50);

        //When I apply Buy one Get One Free on apple
        //And I apply Three For The price of 2 on watermelon
        //And I add apple, watermelon, orange to the DB

        apple.applyOffer();
        watermelon.applyOffer();
        orange.applyOffer();
        underTest.addFruit(apple);
        underTest.addFruit(watermelon);
        underTest.addFruit(orange);


        // Then

        //1) Because we applied offer to apple, quantityAfterOfferApplied of apple will be 7 x 2 = 14
             int quantityAfterOfferAppliedExpected = 14;
             Assert.assertEquals(quantityAfterOfferAppliedExpected, ((Apple) apple).getQuantityAfterOfferApplied());

        //2) Computing the sum of all amountToPay on each type of fruit should match totalToPayExpected
        // With a delta of : PRECISION

        double totalToPayExpected = 8.60000000000000;

        Assert.assertEquals(totalToPayExpected,underTest.processTotal(),PRECISION);

    }


}


