package bnp.assessment.roberttogbe.superette;

import bnp.assessment.roberttogbe.superette.model.Apple;
import org.junit.Assert;
import org.hamcrest.Matchers;
import org.junit.Test;

public class AppleTest {
    @Test
    public void creation() {

        Apple apple = new Apple(2, "myApple2", 4, 0.20);
        Assert.assertEquals(2, apple.getPid());
        Assert.assertThat(apple.getQuantity(), Matchers.equalTo(4));

    }
}
