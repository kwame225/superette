package bnp.assessment.roberttogbe.superette;
import bnp.assessment.roberttogbe.superette.model.Watermelon;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.hamcrest.Matchers;
import org.junit.Test;

public class WatermelonTest {

    @Test
    public void creation() {

        Watermelon watermelon = new Watermelon(3,"melon1",7,0.80);
        Assert.assertEquals(3, watermelon.getPid());
        Assert.assertThat(watermelon.getQuantity(), Matchers.equalTo(7));
        Assertions.assertThat(watermelon.getName()).isEqualTo("melon1");

    }
}
