package bnp.assessment.roberttogbe.superette;

import bnp.assessment.roberttogbe.superette.model.Apple;
import bnp.assessment.roberttogbe.superette.model.Fruit;
import bnp.assessment.roberttogbe.superette.model.Orange;
import bnp.assessment.roberttogbe.superette.model.Watermelon;
import bnp.assessment.roberttogbe.superette.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Scanner;

/**
 * @Author : ROBERT TOGBE - Java/scala/spark/BigData Expert for Industry-leading Corporation
 * @Email : softcons99@gmail.com
 */

//import static java.lang.System.exit;
@SpringBootApplication
public class SuperetteApp implements CommandLineRunner {

    @Autowired
    private  OrderService orderService;


    public static void main(String[] args) throws Exception {
        SpringApplication app = new SpringApplication(SuperetteApp.class);
        app.setBannerMode(Banner.Mode.OFF);
        app.run(args);
    }
    @Override
    public void run(String... args) throws Exception {

        String messageApple = "";
        String messageWater = "";
        String messageOrange = "";
        Scanner input = new Scanner(System.in);
        System.out.print("Please provide the number of Apples ");
        int applenumber = input.nextInt();
        System.out.print("ApplyOffer ? digit 1 for yes or 0 for no: ");
        int appleApply = input.nextInt();
        Fruit apple = new Apple(10, "myapple",applenumber,0.20);
        switch(appleApply) {
            case 0:
                messageApple = "The number of apples offered without offer is : ";
                ((Apple) apple).applyNoOffer();
                break;
            case 1: apple.applyOffer();
                messageApple = "The number of orange after applying BuyOneGetOneFree is : ";
            default:
                break;
        }
        orderService.addFruit(apple);

        System.out.print("Please provide the number of Watermelons ");
        int watermelonnumber = input.nextInt();
        Fruit  watermelon = new Watermelon(20,"melon",watermelonnumber,0.80);
        System.out.print("ApplyOffer ? digit 1 for yes or 0 for no: ");
        int watermelonApply = input.nextInt();
        switch(watermelonApply) {
            case 0:
                messageWater = "The number of watermelons without offer is: ";
                ((Watermelon) watermelon).applyNoOffer();
                break;
            case 1: watermelon.applyOffer();
                    messageWater = "Amount to pay after applying FreeForThePriceOfTwo is : ";
            default:
                break;
        }
        orderService.addFruit(watermelon);
        System.out.print("Please provide the number of Oranges ");
        int orangenumber = input.nextInt();
        Fruit  orange = new Orange(30,"orange",orangenumber,0.50);
        orange.applyOffer();
        orderService.addFruit(orange);

        System.out.println("=================APPLES=================");

        System.out.println(messageApple + ((Apple) apple).getQuantityAfterOfferApplied());
        System.out.println("The initial quantity is : " + apple.getQuantity() );
        System.out.println("The Amount to pay for Apples is: " + apple.getAmountToPay());

        System.out.println("=================WATERMELONS=================");

        System.out.println("The initial quantity of watermelon is : " + watermelon.getQuantity() );
        System.out.println(messageWater + watermelon.getAmountToPay());

        System.out.println("=================ORANGES=================");
        System.out.println("The initial quantity of orange is : " + orange.getQuantity() );
        System.out.println("On Orange, there is no offer so the amount to pay is : " + orange.getAmountToPay());


        System.out.println("=================AMOUNT TO PAY FOR THIS BASKET=================");

        System.out.println("Total to Pay For this basket is : " + orderService.processTotal());
















        /**
            Fruit apple = new Apple(10, "myapple",7,0.20);
            apple.applyOffer();

            System.out.println("==============Apples==================");
            System.out.println("Pid: " +apple.getPid());
            System.out.println("name: " +apple.getName());
            System.out.println("quantity: " +apple.getQuantity());
            System.out.println("qtyAfterOffer : " +((Apple) apple).getQuantityAfterOfferApplied());
            System.out.println("Amount to pay:  " + apple.getAmountToPay());

            System.out.println("Amount to pay:  " + apple.getAmountToPay());
            int ritorno1 = orderService.addFruit(apple);
            System.out.println("Ritorno1: " + ritorno1);

            System.out.println("==============WaterMelons==================");
            Fruit  watermelon = new Watermelon(20,"melon",6,0.80);
            watermelon.applyOffer();
            System.out.println("Pid: " +watermelon.getPid());
            System.out.println("name: " +watermelon.getName());
            System.out.println("quantity: " +watermelon.getQuantity());
            System.out.println("Amount to pay: : " + watermelon.getAmountToPay());
            int ritorno2 = orderService.addFruit(watermelon);
            System.out.println("Ritorno2: " + ritorno2);

        System.out.println("==============Orange==================");

        Fruit  orange = new Orange(30,"orange",8,0.50);
        orange.applyOffer();
        orderService.addFruit(orange);

        System.out.println("name: " +orange.getName());
        System.out.println("quantity: " +orange.getQuantity());
        System.out.println("Amount to pay:  " + orange.getAmountToPay());


        System.out.println("DB Size: " + orderService.getDBSize());



            System.out.println("Total to Pay : " + orderService.processTotal());

            //orderService.processTotal();


**/

    }

}
