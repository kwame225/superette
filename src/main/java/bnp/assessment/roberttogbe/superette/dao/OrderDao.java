package bnp.assessment.roberttogbe.superette.dao;

import bnp.assessment.roberttogbe.superette.model.Apple;
import bnp.assessment.roberttogbe.superette.model.Fruit;
import bnp.assessment.roberttogbe.superette.model.Orange;
import bnp.assessment.roberttogbe.superette.model.Watermelon;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

/**
 * @Author : ROBERT TOGBE - Java/scala/spark/BigData Expert for Industry-leading Corporation
 * @Email : softcons99@gmail.com
 */

public interface OrderDao {

    int addFruit(Fruit fruit);
    public double processTotal();
    public int getDBSize();


}
