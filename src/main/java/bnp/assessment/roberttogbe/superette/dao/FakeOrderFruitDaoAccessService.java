package bnp.assessment.roberttogbe.superette.dao;

import bnp.assessment.roberttogbe.superette.model.Apple;
import bnp.assessment.roberttogbe.superette.model.Fruit;
import bnp.assessment.roberttogbe.superette.model.Orange;
import bnp.assessment.roberttogbe.superette.model.Watermelon;
//import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * @Author : ROBERT TOGBE - Java/scala/spark/BigData Expert for Industry-leading Corporation
 * @Email : softcons99@gmail.com
 */

@Repository("fakeDao") //so we can make use of multiple implementations
public class FakeOrderFruitDaoAccessService implements OrderDao {

    private final List<Fruit> DB = new ArrayList<>();


    public int addFruit(Fruit fruit) {
        getDB().add(fruit);
        return fruit.getPid();
    }

    public void clearDB() {
        getDB().clear();
    }


    public List<Fruit> getDB() {
        return DB;
    }

    /**
    public Optional<Apple> getFruit(int id) {
        return DB
                .stream()
                .filter(fruit -> fruit.getPid().)
    }

     **/

    /**

    public double processTotal2() {

        double total = 0.0;

        for(Fruit f: getDB()) {
            total += f.getAmountToPay();
            System.out.println("monTotalProv : " + total);
        }
        return total;
    }

     **/


    public double processTotal() {

         //System.out.println("primo elemento: " + getDB().get(0).getAmountToPay());

         return getDB().stream().collect(Collectors.summarizingDouble(p -> p.getAmountToPay())).getSum();


        //return getDB().stream().mapToDouble(Fruit::processAmount).sum();

    }

    public int getDBSize() {
        return DB.size();
    }


}
