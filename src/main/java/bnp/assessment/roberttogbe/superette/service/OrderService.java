package bnp.assessment.roberttogbe.superette.service;

import bnp.assessment.roberttogbe.superette.dao.OrderDao;
import bnp.assessment.roberttogbe.superette.model.Apple;
import bnp.assessment.roberttogbe.superette.model.Fruit;
import bnp.assessment.roberttogbe.superette.model.Orange;
import bnp.assessment.roberttogbe.superette.model.Watermelon;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * @Author : ROBERT TOGBE - Java/scala/spark/BigData Expert for Industry-leading Corporation
 * @Email : softcons99@gmail.com
 */

@Service
public class OrderService {


    private final OrderDao orderDao;


    @Autowired
    public OrderService(@Qualifier("fakeDao") OrderDao orderDao) {
        this.orderDao = orderDao;
    }

    public int addFruit(Fruit fruit) {
        return orderDao.addFruit(fruit);
    }

    public int getDBSize() {
        return orderDao.getDBSize();
    }

    public double processTotal() {
        return orderDao.processTotal();
    }



}
