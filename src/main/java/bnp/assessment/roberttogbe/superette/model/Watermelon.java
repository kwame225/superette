package bnp.assessment.roberttogbe.superette.model;

/**
 * @Author : ROBERT TOGBE - Java/scala/spark/BigData Expert for Industry-leading Corporation
 * @Email : softcons99@gmail.com
 */

public class Watermelon extends Fruit implements Offer {

    public Watermelon() {
        super();
    }

    public Watermelon(int id, String name, int quantity, double unitPrice) {
        super(id,name,quantity,unitPrice);
    }

    public Watermelon(int id, String name, int quantity, double unitPrice, double amount) {
        super(id,name,quantity,unitPrice,amount);
    }

    //ThreeForPriceOfTwo
    final int THREE = 3;
    final int TWO = 2;

    int howManyTimesWeFindThreeInQuantity;
    int remainder;
    double priceOfTwo = 2 * getUnitPrice();



    public void applyOffer() {

        howManyTimesWeFindThreeInQuantity = getQuantity() / THREE;
        remainder = getQuantity() % THREE;

        setAmountToPay(howManyTimesWeFindThreeInQuantity * priceOfTwo + remainder * getUnitPrice());

        //return howManyTimesWeFindThreeInQuantity * priceOfTwo + remainder * getUnitPrice();
    }

    public void applyNoOffer() {
        setAmountToPay(getQuantity() * getUnitPrice());
    }


    public int getId() {
        return super.getPid();
    }


}
