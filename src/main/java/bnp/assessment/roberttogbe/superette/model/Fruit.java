package bnp.assessment.roberttogbe.superette.model;

/**
 * @Author : ROBERT TOGBE - Java/scala/spark/BigData Expert for Industry-leading Corporation
 * @Email : softcons99@gmail.com
 */

//import java.util.UUID;

/**
 * Class Fruit is an class that abstract all the attributes
 * and method that are common to other fruits
 */
public abstract class Fruit {


    private int pid; // Fruit ID
    private String fname; // Fruit name
    private int quantity; // Fruit quantity
    private double unitPrice; // Fruit unit price
    private double amountToPay; // Fruit amount

    public double getAmountToPay() {
        return amountToPay;
    }

    public void setAmountToPay(double amountToPay) {
        this.amountToPay = amountToPay;
    }

    public Fruit() {

    }

    /**
     *
     * @param pid
     * @param name
     * @param quantity
     * @param unitPrice
     */
    public Fruit(int pid,String name, int quantity, double unitPrice) {
        this.pid = pid;
        this.fname = name;
        this.quantity = quantity;
        this.unitPrice = unitPrice;
    }

    /**
     *
     * @param pid
     * @param name
     * @param quantity
     * @param unitPrice
     * @param amount
     */
    public Fruit(int pid,String name, int quantity, double unitPrice, double amount) {
        this.fname = name;
        this.quantity = quantity;
        this.unitPrice = unitPrice;
        this.amountToPay = amount;
    }


    public int getPid() {
        return pid;
    }
    public void setPid(int pid) {
        this.pid = pid;
    }

    public String getName() {
        return fname;
    }

    public void setName(String name) {
        this.fname = name;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double processAmount(){
        return getAmountToPay();
    }


    public void applyOffer() {

    }



}
