package bnp.assessment.roberttogbe.superette.model;

/**
 * @Author : ROBERT TOGBE - Java/scala/spark/BigData Expert for Industry-leading Corporation
 * @Email : softcons99@gmail.com
 */

public class Orange extends Fruit {

    public Orange(int id, String name, int quantity, double unitPrice) {
        super(id,name,quantity,unitPrice);
    }

    public Orange(int id, String name, int quantity, double unitPrice, double amountToPay) {

        super(id,name,quantity,unitPrice,amountToPay);
    }

    public void applyOffer() {
        super.setAmountToPay(getQuantity() * getUnitPrice());
    }

    public int getId() {
        return super.getPid();
    }


}
