package bnp.assessment.roberttogbe.superette.model;

/**
 * @Author : ROBERT TOGBE - Java/scala/spark/BigData Expert for Industry-leading Corporation
 * @Email : softcons99@gmail.com
 */

public interface Offer {

    //public void setAmountAfterOfferApplied();
    public void applyOffer();

}
