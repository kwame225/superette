package bnp.assessment.roberttogbe.superette.model;

//import java.util.UUID;

/**
 * @Author : ROBERT TOGBE - Java/scala/spark/BigData Expert for Industry-leading Corporation
 * @Email : softcons99@gmail.com
 */

//Class Apple represents the modelisation of an apple fruit
public class Apple extends Fruit implements Offer {
    // attributes
    private int quantityAfterOfferApplied; // quantity in hands after offer have been applied

    public int getQuantityAfterOfferApplied() {
        return quantityAfterOfferApplied;
    }

    public void setQuantityAfterOfferApplied(int quantityAfterOfferApplied) {
        this.quantityAfterOfferApplied = quantityAfterOfferApplied;
    }

    /**
     *
     * @param id the id of Apple
     * @param name the name of the Apple
     * @param quantity the quantity requested
     * @param unitPrice unit price of an apple
     */
    public Apple(int id, String name, int quantity, double unitPrice) {
        super(id, name,quantity,unitPrice);
    }

    /**
     *
     * @param id
     * @param name
     * @param quantity
     * @param unitPrice
     * @param amount
     */
    public Apple(int id, String name, int quantity, double unitPrice, double amount) {
        super(id, name,quantity,unitPrice, amount);
    }

    /**
     *
     * @param id
     * @param name
     * @param quantity
     * @param unitPrice
     * @param qtyAfterOffer the quantity in hands after offer has been applied
     */
    public Apple(int id, String name, int quantity, double unitPrice, int qtyAfterOffer) {

        //new Apple(id,name,quantity,unitPrice);
        super(id,name,quantity,unitPrice);
        this.setQuantityAfterOfferApplied(qtyAfterOffer);

    }


    public void applyOffer() {
        this.setQuantityAfterOfferApplied(2 * getQuantity());
        setAmountToPay(getQuantity() * getUnitPrice());
    }

    public void applyNoOffer() {
        setAmountToPay(getQuantity() * getUnitPrice());
    }


    public int getId() {
        return super.getPid();
    }


}
